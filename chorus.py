#!/usr/bin/env python

# http://www.sengpielaudio.com/Rechner-notennamen.htm
# http://www.clavio.de/Notennamen.777.0.html

from __future__ import division
import midiutil.MidiFile as MidiFile
import re
import os

INPUT_FILE_NAME = r'scores/deutsche_motetten.pmd'
#INPUT_FILE_NAME = r'scores/test.pmd'
MPM = 110
MEASURE = 4, 4
MEASURE_RATIO = MEASURE[0]/MEASURE[1]
BPM = MPM * MEASURE_RATIO
ACTIVE_VOICES = (1, 2, 3, 4, 5, 6)
ACTIVE_VOICES = (3, 6)
ACTIVE_VOICES = (1,)
INSTRUMENTS   = () #(0, 8, 11) # 0, 8, 11
START_MEASURE = 19
END_MEASURE = 49


class TempoError(BaseException):
    pass

PITCH_TO_NUM = {'C' :  0, 'B#':  0,
                'C#':  1, 'DB':  1,
                'D' :  2,
                'D#':  3, 'EB':  3,
                'E' :  4, 'FB':  4,
                'F' :  5, 'E#':  5,
                'F#':  6, 'GB':  6,
                'G' :  7,
                'G#':  8, 'AB':  8,
                'A' :  9,
                'A#': 10, 'BB': 10,
                'B' : 11, 'CB': 11,
                'P': -9999}


MAX_N_VOICES = 16
START_MEASURE -= 1
END_MEASURE   -= 1
max_n_measures = 0

midi = MidiFile.MIDIFile(1)
midi.addTrackName(0, 0, os.path.basename(INPUT_FILE_NAME).split('.')[0])
midi.addTempo(0, 0, int(BPM))

for current_voice in ACTIVE_VOICES:
    current_measure = 0
    measure_length = 0
    previous_octave = None
    current_time = 0

    for voiceLine in open(INPUT_FILE_NAME, 'r'):
        if voiceLine.startswith('%i:' % (current_voice,)):
            for match in re.finditer(r'([cdefgabhp][#b\+]?)(\d?)\s+(\d+\.?)\s+(:?\|?:?)', voiceLine, re.IGNORECASE):
                print match.groups()

                duration = match.group(3)
                if duration.endswith('.'):
                    duration = 1/float(duration)/MEASURE_RATIO*1.5
                else:
                    duration = 1/float(duration)/MEASURE_RATIO
                duration *= MEASURE[0]

                pitch = match.group(1)[0].upper().replace('H', 'B') + match.group(1)[1:]

                ########
                if pitch.endswith('+'):
                    pitch = pitch[0]
                else:
                    if pitch == 'B': pitch = 'Bb'
                    #if pitch == 'E': pitch = 'Eb'
                    #if pitch == 'A': pitch = 'Ab'

                    #if pitch == 'F': pitch = 'F#'

                    #if pitch == 'C': pitch = 'C#'
                    #if pitch == 'G': pitch = 'G#'
                    #if pitch == 'D': pitch = 'D#'
                ########

                pitch = PITCH_TO_NUM[pitch.upper()]

                try:
                    octave = int(match.group(2))
                    previous_octave = octave
                except ValueError:
                    octave = previous_octave

                #if current_voice == 4: octave -= 1

                print('%2s %2i %2i (%4.1f)' % (pitch, octave, duration, current_time))

                if START_MEASURE <= current_measure <= END_MEASURE:
                    if pitch >= 0:
                        midi.addNote(0, current_voice, 12*octave+pitch+12, current_time, int(round(duration)), 127)
                    current_time += duration

                measure_length += duration

                if match.group(4) != '':
                    if int(round(measure_length)) == MEASURE[0]:
                        if False and match.group(4).startswith(':'):
                            repeat_end = current_measure
                            for m in xrange(repeatStart, repeatEnd+1):
                                current_measure += 1
                                #voice.DuplicateMeasure(m)
                                #rm = voice.NewMeasure(currentMeasureIndex)
                                #newMeasure = voice.NewMeasure(currentMeasureIndex)
                                #newMeasure.notes = voice.measures[m].notes
                                #print m, len(voice.measures[-1].

                        current_measure += 1

                        print('------------------------------')
                    else:
                        raise TempoError('incomplete measure')
                    measure_length = 0
                else:
                    if int(round(measure_length)) > MEASURE[0]:
                        raise TempoError('overfull measure')

                if match.group(4).endswith(':'):
                    repeat_start = current_measure

    max_n_measures = max(max_n_measures, current_measure)

# add beat
for current_time in xrange(0, max_n_measures*MEASURE[0]):
    midi.addNote(0, 9, 76, current_time, 1, 127 if current_time % 4 == 0 else 70)

midi.writeFile(open("output.mid", 'wb'))

print('done')
