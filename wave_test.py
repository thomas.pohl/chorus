#!/usr/bin/env python

from __future__ import division
import pygame.mixer as mixer
import pygame.sndarray as sndarray
import numpy
import math
import time

mixer.init(44100//2, channels=1) # mono
#mixer.set_num_channels(16)
SAMPLING_FREQ, FORMAT, N_CHANNELS = mixer.get_init()
print SAMPLING_FREQ

PITCH_FAC = 2**(1/12)
PITCH_A = 27.5
PITCH_C = PITCH_A*(PITCH_FAC**-9)

def resample(wav, f):
    # better B-spline interpolation: http://www.scipy.org/Cookbook/Interpolation
    left  = numpy.interp(numpy.linspace(0, wav.shape[0], f*wav.shape[0], False),
                         numpy.arange(wav.shape[0]), wav[:, 0])
    right = numpy.interp(numpy.linspace(0, wav.shape[0], f*wav.shape[0], False),
                         numpy.arange(wav.shape[0]), wav[:, 1])
    resampled_wav = numpy.dstack((left, right))
    resampled_wav.shape = (f*wav.shape[0], 2)
    return numpy.round(resampled_wav).astype(numpy.int16)


def piano_sample():
    piano0 = mixer.Sound('OpenPathMusic44V4/piano-studio-octave0.wav')
    piano1 = mixer.Sound('OpenPathMusic44V4/piano-studio-octave1.wav')
    piano2 = mixer.Sound('OpenPathMusic44V4/piano-studio-octave2.wav')

    for i in (0, 2, 4, 5, 7, 9, 11, 12):#xrange(13):
        wav = sndarray.array(piano1)
        wav = resample(wav, 1.0/(2**(i/12.)))
        piano = sndarray.make_sound(wav)
        print('play')
        piano.play()
        while mixer.get_busy(): time.sleep(0.1)

def create_sample(tone_freq, tone_dur, tone_vol, tone_decay_dur=0.5):
    MAX_WAVE_AMP = 2**11

    envelope_tone = numpy.linspace(0, tone_dur, SAMPLING_FREQ*tone_dur, True)
    envelope_tone = 1/2**envelope_tone
    envelope_decay = numpy.linspace(0, tone_decay_dur, SAMPLING_FREQ*tone_decay_dur, True)
    envelope_decay = envelope_tone[-1]/2**(20*envelope_decay)
    envelope = numpy.hstack((envelope_tone, envelope_decay))

    wave = numpy.linspace(0, tone_dur+tone_decay_dur, (tone_dur+tone_decay_dur)*SAMPLING_FREQ, True)

    # sine
    #wave = MAX_WAVE_AMP*numpy.sin((2*numpy.pi*tone_freq)*wave)

    # saw tooth
    #wave = MAX_WAVE_AMP*(numpy.mod((2*tone_freq)*wave+1, 2)-1)

    # square
    #wave = 2*MAX_WAVE_AMP*(numpy.mod(numpy.round(tone_freq*wave), 2)-0.5)

    # triangle
    wave = (numpy.mod((4*tone_freq)*wave+1, 4)-1)
    mask = (1 < wave) & (wave < 3)
    wave[mask] = 2-wave[mask]
    wave *= MAX_WAVE_AMP

    wave *= envelope
    wave = numpy.round(wave).astype(numpy.int16)

    # make stereo
    #piano = numpy.dstack((wave, wave))
    #piano.shape = (wave.shape[0], 2)

    sound = sndarray.make_sound(wave)
    return sound
    #print('play')
    #piano.play()
    #while mixer.get_busy(): time.sleep(0.1)

def create_samples():
    samples = {}
    for dur_str in ('1', '2', '2.', '4', '4.', '8', '8.', '16', '16.', '32', '32.'):
        dur = 1/float(dur_str)
        if dur_str.endswith('.'): dur *= 1.5
        print dur
        for octave in xrange(3, 7):
            for pitch in xrange(12):
                pitch_freq = PITCH_C*(2**octave)*(PITCH_FAC**pitch)
                #print pitch_freq, dur
                sample = create_sample(pitch_freq, dur, 1)
                samples[octave, pitch, dur_str] = sample
                if dur_str == 'xxx32':
                    sample.play()
                    while mixer.get_busy(): time.sleep(0.05)
        #break
    return samples

if __name__ == '__main__':
    #piano_sample()
    samples = create_samples()
    print('done')
