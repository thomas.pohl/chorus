#!/usr/bin/env python

import random
import multiprocessing as mp
#random.seed(0)
import sys

N_BALLS = 120#  12 balls => 3 questions
              #  39 balls => 4 questions
              # 120 balls => 5 questions
              # 361 balls => 6 questions


choose = lambda s, n: set(random.sample(s, n))


def print_status(pl, ph):
    print('### STATUS ###')
    print('pl: %s' % pl)
    print('ph: %s' % ph)
    print('##############')


def find_best_comparison(al, pl, ph):
    N_BALLS_HALF = N_BALLS // 2

    npl1 = len(pl-ph)
    nph1 = len(ph-pl)
    npb1 = len(pl&ph)
    npo1 = len(al-(pl|ph))

    best_diff_min = 0
    best_comparison = None

    for cpl1 in range(0, npl1+1):
        if cpl1 > N_BALLS_HALF: break
        for cph1 in range(0, nph1+1):
            if cpl1+cph1 > N_BALLS_HALF: break
            for cpb1 in range(0, npb1+1):
                if cpl1+cph1+cpb1 > N_BALLS_HALF: break
                if cpl1+cph1+cpb1 == 0: continue

                npl2 = npl1-cpl1
                nph2 = nph1-cph1
                npb2 = npb1-cpb1
                npo2 = npo1

                for cpl2 in range(0, npl2+1):
                    for cph2 in range(0, nph2+1):
                        for cpb2 in range(0, npb2+1):
                            cpo2 = (cpl1+cph1+cpb1)-cpl2-cph2-cpb2
                            if not 0 <= cpo2 <= npo2: continue

                            npl3 = npl2-cpl2
                            nph3 = nph2-cph2
                            npb3 = npb2-cpb2
                            #npo3 = npo2-cpo2

                            diff_min = min(cpl1+cph1+2*cpb1 + cpl2+cph2+2*cpb2,
                                           cpl1+cpb1 + cph2+cpb2 + npl3+nph3+2*npb3,
                                           cph1+cpb1 + cpl2+cpb2 + npl3+nph3+2*npb3)

                            if diff_min > best_diff_min:
                                #best_comparison = (cpl1, cph1, cpb1, 0, cpl2, cph2, cpb2, cpo2)
                                best_comparison = dict(n_pl_only_left=cpl1,
                                                       n_ph_only_left=cph1,
                                                       n_pb_left=cpb1,
                                                       n_pl_only_right=cpl2,
                                                       n_ph_only_right=cph2,
                                                       n_pb_right=cpb2,
                                                       n_others_right=cpo2)
                                best_diff_min = diff_min
    return best_comparison


def game(n_balls, answer_callback):
    al = set(range(1, n_balls+1))
    pl = al.copy()
    ph = al.copy()

    n_questions = 0
    while len(pl)+len(ph) > 1:
        best_comparison = find_best_comparison(al, pl, ph)

        pl_only = pl - ph
        ph_only = ph - pl

        scale_left = choose(pl_only, best_comparison['n_pl_only_left'])
        scale_left |= choose(ph_only, best_comparison['n_ph_only_left'])
        scale_left |= choose(pl & ph, best_comparison['n_pb_left'])

        scale_right = choose(pl_only - scale_left, best_comparison['n_pl_only_right'])
        scale_right |= choose(ph_only - scale_left, best_comparison['n_ph_only_right'])
        scale_right |= choose((pl & ph) - scale_left, best_comparison['n_pb_right'])
        scale_right |= choose((al - (pl | ph)) - scale_left, best_comparison['n_others_right'])

        answer = answer_callback(scale_left, scale_right)

        if answer == 'e':
            pl -= scale_left | scale_right
            ph -= scale_left | scale_right
        elif answer == 'l':
            pl -= al - scale_right
            ph -= al - scale_left
        elif answer == 'r':
            pl -= al - scale_left
            ph -= al - scale_right

        n_questions += 1

    return n_questions, 'ball %s is %s' % ((pl|ph).pop(), 'lighter' if pl else 'heavier')


def test(ball):
    def answer(scale_left, scale_right):
        r = 'e'
        if ball in scale_left:
            r = 'l' if heavier else 'r'
        elif ball in scale_right:
            r = 'r' if heavier else 'l'

        return r

    n_questions = 0
    for heavier in (False, True):
        n_questions = max(n_questions, game(N_BALLS, answer)[0])

    return n_questions


def test_all():
    import time

    with mp.Pool(8) as pool:
        start_time = time.time()
        max_questions = max(pool.map(test, range(1, N_BALLS+1)))
        end_time = time.time()

    print('max_questions: %i; time: %f' % (max_questions, end_time-start_time))


def manual():
    def compare(scale_left, scale_right):
        while True:
            print('\nleft: ' + str(sorted(scale_left)) + '\nright: ' + str(sorted(scale_right)) + ' ? ')
            try:
                answer = sys.stdin.readline().strip()[0]
                assert answer in 'lre'
            except:
                print('invalid answer')
            else:
                return answer

    n_questions, result = game(N_BALLS, compare)
    print('\nresult: ' + str(result))


if __name__ == '__main__':
    #test_all()
    manual()
    print('done')
